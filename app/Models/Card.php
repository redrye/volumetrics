<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'contents'
    ];

    public function view() {
        return $this->belongsTo(View::class, 'views_id');
    }
}
