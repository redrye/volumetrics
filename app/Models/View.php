<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    use HasFactory;

    protected $attributes = [
        'id',
        'name'
    ];

    public function cards() {
        return $this->hasMany(Card::class, 'views_id');
    }
}
