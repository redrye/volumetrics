<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckMake extends Model
{
    use HasFactory;

    public function trucks() {
        return $this->hasMany(Truck::class);
    }
}
