<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    use HasFactory;

    protected $fillable = [

    ];

    public function model() {
        return $this->belongsTo(TruckModel::class);
    }

    public function make() {
        return $this->belongsTo(TruckMake::class);
    }
}
