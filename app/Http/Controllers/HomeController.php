<?php

namespace App\Http\Controllers;

use App\Models\View as V;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if($request->ajax()) {

        } else {
            $view = V::find(1);
            return view('pages.home', [
                'view' => $view
            ]);
        }
    }
}
