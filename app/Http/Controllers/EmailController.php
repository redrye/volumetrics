<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:13'
        ]);
        if($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        $email_from="From: $request->name<$request->email_address>\r\nReturn-path: $request->email_address";
        $email_subject="vmbystrong.com Website Inquiry";

        $message = str_replace("\\r", "", $request->message);
        $message = str_replace("\\n", "[newline]", $message);
        $message = stripslashes($message);
        $message = str_replace("[newline]", "\n", $message);
        $email_body = "vmbystrong.com Inquiry\n==========================\nDATE: ".date("m/d/Y g:i a", strtotime("now"))."\n\nNAME: $request->name\nPHONE: $request->phone_number\nEMAIL: $request->email_address\n\nMESSAGE / INQUIRY:\n---------------------------\n$message\n---------------------------\n";
        if (mail("lorri@superdumps.com", $email_subject, $email_body, $email_from)) {
             return response('success');
            } else {
                return response(error_get_last(),402);
            }

    }
}
