<?php

namespace App\Http\Controllers;

use App\Models\View as V;
use Illuminate\Http\Request;

class MixerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function __invoke()
    {
        return view('pages.mixers.index', [
            'view' => V::find(1)
        ]);
    }

}
