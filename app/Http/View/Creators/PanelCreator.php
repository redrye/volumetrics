<?php


namespace App\Http\View\Creators;


use Illuminate\View\View;

class PanelCreator
{
    public function __construct()
    {

            $this->header = '';
            $this->headerClass = 'card-header bg-info';
            $this->bodyClass = 'card-body';
    }

    public function create(View $view)
    {
        $view->with('attributes', $this->headerClass);
    }
}