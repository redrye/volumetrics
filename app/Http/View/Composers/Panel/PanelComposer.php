<?php
namespace App\Http\View\Composers\Panel;

use Illuminate\View\View;

class PanelComposer
{
    public function __construct()
    {
        $this->header = '';
        $this->headerClass = 'card-header bg-info';
    }

    public function compose(View $view)
    {
        $view->with('count', $this);
    }
}