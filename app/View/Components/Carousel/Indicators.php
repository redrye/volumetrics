<?php

namespace App\View\Components\Carousel;

use Illuminate\View\Component;

class Indicators extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return <<<'blade'
<div {{$attributes->merge(['class' => 'carousel-indicators'])}}">
@for ($i = 0; $i < $attributes['num']; $i++)
<button type="button" data-bs-target="#{{$attributes['target']}}" data-bs-slide-to="{{ $i }}" @if($i == 0) class="active" aria-current="true" @endif aria-label="Slide {{ $i + 1 }}"></button>
@endfor
</div>
blade;
    }
}
