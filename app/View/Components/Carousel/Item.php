<?php

namespace App\View\Components\Carousel;

use Illuminate\View\Component;

class Item extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return <<<'blade'
 <div class="carousel-item {{ isset($attributes['active']) ? ' active' : '' }}">
             <div class="{{  $attributes['class'] . ' carousel-caption d-none d-md-flex flex-column w-100'}}" style="left:0; right:0">
                <h3>{{$attributes['caption-title']}}</h3>
                <h4"><i>{{$attributes['caption-text']}}</i></h4>
            </div>
            <img src="/images/{{ isset($attributes['namespace']) ? $attributes['namespace'] . '/' : '' . $attributes['image']}}" class="d-block w-100" alt="...">

        </div>
blade;
    }
}
