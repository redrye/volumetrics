const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const dom = new JSDOM('',  {
    url: "https://www.foxebook.net",
  contentType: "text/html",
  includeNodeLocations: true,
  storageQuota: 10000000
});
const $ = (require('jquery'))(dom.window)

$.ajaxSetup({ 'access-control-allow-origin' : 'https://www.foxebook.net'})
/*
$.ajax({
    url: 'https://www.foxebook.net',
    crossDomain: true,
    success: function(data) {
        console.log(data)
    }
})
*/
console.log(dom.window.document);
