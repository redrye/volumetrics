#!/usr/bin/env python3


import requests, codecs


class Downloader:
	def __init__(self, url=None):
		self.url = 'https://readthedocs.org/projects/python-guide/downloads/pdf/latest/'
		self.list = []
		self.files = []
	def download(self, url=None, redirects=True):
		if(url != None):
			self.url = url
			
		myfile = requests.get(url, allow_redirects=redirects)
		open('hello.pdf', 'wb').write(myfile.content)
		
	def buildList(self):
		myfile = requests.get(self.url, allow_redirects=True)
		print(myfile.content)
	def getFiles(self):
		return self.files
		
if __name__ == '__main__':
	d = Downloader();
	d.buildList()
	
