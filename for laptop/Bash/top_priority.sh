#!/usr/bin/env bash

count=45;

renice -n -20 -p $(ps -A | grep $1 | cut -d ? -f 1 | sed 's/\s//g')

echo -e "\033[1A\033[$(($(stty size | cut -d ' ' -f1)))C [\033[1;34m Done \033[1;0m]\033"
