<?php

namespace Database\Seeders;

use App\Models\View;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;

class AddViews extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('views')->insert([
            [
                'id' => 1,
                'name' => 'Home',
            ], [
                'id' => 2,
                'name' => 'About'
            ], [
                'id' => 3,
                'name' => 'Videos'
            ],
            [
                'id' => 4,
                'name' => 'Contact'
            ], [
                'id' => 5,
                'name' => 'About.Strengths'
            ],
            [
                'id' => 6,
                'name' => 'About.Gunite'
            ],
            [
                'id' => 7,
                'name' => 'About.History'
            ]

        ]);
        DB::table('cards')->insert([
            [
                'views_id' => 1,
                'title' => 'VETERAN DESIGN',
                'contents' => 'Brooks Strong’s many years of experience in the gunite contracting business taught him the importance of reliability, performance, and ease of maintenance. These central tenets were his primary considerations when he designed the Strong Gunite Mixer.'
            ],
            [
                'views_id' => 1,
                'title' => 'PEAK EFFICIENCY',
                'contents' => 'Strong Gunite Mixers can be easily operated by one person, are built to last and easy to maintain, come with a confident service warranty, and can be equipped with a host of productivity-boosting options to leave your competition in the dust.'
            ],
            [
                'views_id' => 1,
                'title' => 'BETTER MIXER. BETTER SERVICE. BETTER VALUE.',
                'contents' => 'These three reasons are why 95% of gunite mixers sold are Strong Mixers. For over three decades, Strong has been pioneering mixer designs and manufacturing methods to achieve maximum efficiency. The gunite machines we offer today represent the culmination of decades of experience and refinement in producing the most cost effective gunite mixers in the industry. '
            ],
            [
                'views_id' => 1,
                'title' => 'LETS GET TO WORK',
                'contents' => ''
            ],
            [
                'views_id' => 5,
                'title' => '',
                'contents' => '<h1 class="text-nowrap text-secondary text-center" style="font-size: 64px; font-family: Alfa Slab One"><b>STRONG GUNITE SYSTEMS</b></h1><h2 class="text-center" style="font-family: Alfa Slab One"><b>LEADING THE INDUSTRY</b></h2><div class="card-body justify-content-center align-items-center d-flex flex-column bg-secondary py-5"><h2 class=" my-3 text-white text-center" style="font-family: Alfa Slab One"><b>Let\'s Get To Work!</b></h2><a class="col-3 btn btn-secondary border-1 border-white" href="/contact"><h2><b>Contact Us</b></h2></a></div>'
            ],
            [
                'views_id' => 6,
                'title' => 'But what is Gunite?',
                'contents' => '<div class="d-flex flex-row h-100 justify-content-between"><div class="d-flex flex-column mx-3 w-50 align-items-center"><p>
Gunite is dry sand and cement mixed and conveyed pneumatically to a shooting nozzle. Water is added as the mixture is shot directly on the point of application. The resultant concrete has a high cement-to-water ratio and high compaction, making it stronger than most other forms of concrete.</p><img class="my-4" src="/images/what is gunite/1.png" /><p>Gunite and Shotcrete are two trade names that refer to the technical terms; ‘Dry Shotcrete’ and ‘Wet Shotcrete’ respectively. Both are methods of of spraying concrete, but the two techniques use significantly different processes to accomplish the same goal, and as such have different rates of success.<br/><br/>

In brief: The Gunite method keeps the concrete components separated until the moment of application. This eliminates many of the problems that plague Wet Shotcrete, and is ideal for complex jobs like a well designed swimming pool, important structures for which structural failure is not an option, and structures that are expected to last more than a few months. Because there is no ‘ticking clock’ for the gunite method, gunite affords concrete workmen the time they need to do their jobs properly instead of racing against the setting of Wet Shotcrete or fighting against the practical logistics of performing a job in the real world.<br/><br/>

Wet Shotcrete (or just ‘Shotcrete’) can be applied faster and is generally acceptable for small, simple, uncomplicated jobs where the cost and consequences for failure are low and the structure is not expected to remain structurally sound for more than a few months. It’s successful application relies on a worryingly large number of things ‘going right’, but under ideal conditions a structure shot with Wet Shotcrete can sometimes approach the quality, integrity, and endurance of a Gunite structure. </p></div><div class="d-flex flex-column w-50 mx-3"><iframe class="card-img flex-grow-1 mb-2" src="https://www.youtube.com/embed/Ugq-Vmp_Ot0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><img class="card-img" src="/images/what is gunite/2.png" /></div></div><div class="my-5 d-flex align-items-center justify-content-center"><div class="col-8 d-flex flex-column align-items-center"><h4 class="my-5 text-nowrap text-secondary text-center" style=" font-family: Alfa Slab One"><b>Your Single Source For Complete Gunite System</b></h4><p><a href="/history">Our extensive experience</a> in the Gunite industry and with Gunite contractors of all kinds makes us familiar with businesses of all sizes. Strong can be your single source for complete Gunite systems and can anticipate your needs with a brief consultation.</p><a class="d-inline-flex btn btn-white border-1 border-secondary" href="/contact"><h4 class="text-secondary m-2"><b>Lets Get To Work!</b></h4></a></div>'
            ],[
                'views_id' => 6,
                'title' => 'Our Equipment',
                'contents' => '<div class="d-flex flex-row h-100 justify-content-between">
<div class="d-flex flex-column mx-3 w-25 align-items-center">
<img class="img-fluid image-thumbnail" src="/images/equipment/material_bin.jpeg" />
</div>
<div class="d-flex flex-column w-25 mx-3">
</div>
</div>
<div class="my-5 d-flex align-items-center justify-content-center">
<div class="col-8 d-flex flex-column align-items-center">
<h4 class="my-5 text-nowrap text-secondary text-center" style=" font-family: Alfa Slab One">
<b>Your Single Source For Complete Gunite System</b>
</h4>
<p>
<a href="/history">Our extensive experience</a> in the Gunite industry and with Gunite contractors of all kinds makes us familiar with businesses of all sizes. Strong can be your single source for complete Gunite systems and can anticipate your needs with a brief consultation.</p>
<a class="d-inline-flex btn btn-white border-1 border-secondary" href="/contact"><h4 class="text-secondary m-2">
<b>Lets Get To Work!</b>
</h4>
</a>
</div>'
            ],

        ]);
    }
}
