<?php

namespace Database\Seeders;

use App\Models\TruckMake;
use Illuminate\Database\Seeder;

class TruckMakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $makes = [
            'International',
            'Mack',
            'Peterbilt',
            'Kensworth',
            'Volvo'
        ];
        foreach($makes as $make) {
            $truckmake = new TruckMake(['name' => $make]);
            $truckmake->save();
        }
    }
}
