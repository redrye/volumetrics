<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-dark position-fixed sticky-top w-100">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home.index') }}">VM By Strong</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <x-navbar.collapse id="navbarSupportedContent">
            <x-navbar.list class="mb-2 mb-lg-0">
                <x-navbar.nav-link href="{{ route('mixers.index') }}">Gunite Mixers</x-navbar.nav-link>
                <x-navbar.nav-link href="{{ route('about.strengths') }}">Our Strengths</x-navbar.nav-link>
                <x-navbar.nav-link href="{{ route('about.gunite') }}">What is Gunite?</x-navbar.nav-link>
                <x-navbar.nav-link href="{{route('history.index')}}">History</x-navbar.nav-link>
                <x-navbar.nav-link href="{{route('videos.index')}}">Videos</x-navbar.nav-link>
                <x-navbar.nav-link aria-current="page" href="{{route('contact.index')}}">Contact Us</x-navbar.nav-link>
            </x-navbar.list>
        </x-navbar.collapse>
    </div>
</nav>
