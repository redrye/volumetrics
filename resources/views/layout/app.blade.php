<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.meta')
    <title>Laravel</title>

    @include('layout.css')
</head>
<body class="antialiased bg-dark">
@include('layout.nav')
<main class="container-fluid my-0 mx-0 px-0">
@yield('content')
</main>
</body>
@include('layout.js')
@push('scripts')

@endpush
