<x-container-fluid class=" px-2 py-5" bgSrc="/images/infrastruture.png">
    <x-container class="my-4 pt-4 pb-4 h-50">
        <x-card.card class=" mb-4 mt-5">
            <x-card.header class="d-flex flex-row">
                <x-card.title>
                    Intellitrucks - Truck and job management at your fingertips
                </x-card.title>
            </x-card.header>
            <x-card.body class="card-body p-0">
                <div id="carousel" class="carousel slide" data-bs-ride="carousel">
                    <x-carousel.indicators target="carousel" num="10" class="mx-0 bg-info mb-0 pb-2"></x-carousel.indicators>
                    {{--<div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>--}}
                    <div class="carousel-inner">
                        <x-carousel.item active="true"
                                         image="/applications/dashboard.png"
                                         caption-title="Google Maps Powered"
                                         caption-text="Interactive maps to view jobs in various locations."
                                         class="bg-info">
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/tables.png"
                            caption-title="Datatables for organization"
                            caption-text="Clean and consistent data representation for all data." bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                                image="applications/tables.png"
                                caption-title="Propriatary API specifically designed for load monitoring"
                                caption-text="" bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/truck_view.png"
                            caption-title="View your truck's current position and edit it's details on the fly."
                            caption-text="" bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/issue_and_job_history.png"
                            caption-title="Interactive table for viewing and editing user information"
                            caption-text="Add or Suspend users with just a few short keystrokes" bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/job_view.png"
                            caption-title="Simple and clean interface for all graph data."
                            caption-text="View and edit your job details and job location." bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/job_view.png"
                            caption-title="Simple and clean interface for all graph data."
                            caption-text="View and edit your job details and job location." bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/client_viewer_owner.png"
                            caption-title="Take advantage of our simple yet clean interface for your editing needs"
                            caption-text="Client details and Viewers of specific jobs may be edited or a job changed to a new owner." bg-white>
                        </x-carousel.item>
                        <x-carousel.item
                            image="applications/truck_assignment.png"
                            caption-title="Take advantage of our simple yet clean interface for your editing needs"
                            caption-text="Trucks as well." bg-white>
                        </x-carousel.item>
                    </div>
                </div>
            </x-card.body>
        </x-card.card>
    </x-container>
</x-container-fluid>
