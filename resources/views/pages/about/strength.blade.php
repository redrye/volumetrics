<div class="py-4"></div>
@include('pages.mixers.viewer')
{{--    <x-flex-row class="justify-content-center">
    <div class="card-group mt-3 col-10">
        <x-card.card class="mx-3">
            <x-card.header >
                <x-card.title>
                    User Friendly Controls
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between my-0 h-100">
                <img class='card-img flex-shrink-1 bg-black' src="/images/controls.png" />
                <x-card.card class="w-50 flex-fill">
                    <x-card.body>Simple Levers and Hydraulic Circuits provide virtually effortless control of all functions of the mixer. </x-card.body>
                </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
        <x-card.card class="mx-3">
            <x-card.header >
                <x-card.title>
                    Maximum Payload
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">

                <img class='card-img w-75' src="/images/max_payload.png" />
                <x-card.card class="w-25">
                    <x-card.body>Simple Levers and Hydraulic Circuits provide virtually effortless control of all functions of the mixer. </x-card.body>
                </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>

        <x-card.card class="mx-3">
            <x-card.header >
                <x-card.title>
                    Optimized Design
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">

                <img class='card-img w-75' src="/images/optimized_design.png" />
                <x-card.card class="w-25">
                    <x-card.body>Simple Levers and Hydraulic Circuits provide virtually effortless control of all functions of the mixer. </x-card.body>
                </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
    </div>
    </x-flex-row>
    <x-flex-row class="justify-content-center w-100 mt-3 mb-5">
        <x-card.card class="mt-5 col-10">
            <x-card.header >
                <x-card.title>
                    Your Single Source for Complete Gunite Systems
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">
                    <img class='card-img w-25' src="/images/compressor_setup.png" />
                    <x-card.card class="w-75">
                            <x-card.body>
                                <p>
                                    <a href="{{ route('history.index') }}">We have experience in the gunite industry.</a> From contractor work to manufacturing, even going back to our franchising days. This means we know what to expect with every facet of the business and can supply new clients and seasoned veterans alike with every piece of equipment a gunite business needs, all held to the highest standards.</p>
                                    <x-card.card :card="$view->cards[0]" class="flex-fill">
                                    </x-card.card>

                            </x-card.body>
                        </x-card.card>
                    </x-flex-row>
            </x-card.body>
        </x-card.card>
    </x-flex-row>--}}
