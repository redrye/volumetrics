<x-container-fluid class="h-100 mx-0 border-0 mt-5 px-0 flex-column" bgSrc="/images/infrastruture.png">
    <x-flex-row class="justify-content-center mx-auto">
        <x-card.card class="mt-5 col-12 col-md-4">
            <x-card.header class="d-flex flex-row">
                <x-card.title>
                    The Strong Gunite Advantage
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row>
                    <img class='card-img w-75 flex-shrink-1' src="/images/truck.png" />
                    <x-flex-column>
                        <x-card.card class="flex-grow-1">
                            <x-card.header class="bg-light">
                                <h4 class="card-title">Capacities</h4>
                            </x-card.header>
                            <x-card.body class="px-2">
                                <ul>
                                    <li>Cement: 85ft<sup>3</sup></li>
                                    <li>Sand: 378ft<sup>3</sup></li>
                                    <li>Total: 14yds<sup>3</sup></li>
                                </ul>
                            </x-card.body>
                        </x-card.card>

                        <x-card.card class="flex-grow-1">
                            <x-card.header class="bg-light">
                                <h4 class="card-title">Dimensions</h4>
                            </x-card.header>
                            <x-card.body class="px-2">
                                <ul>
                                    <li>Cement: 97'</li>
                                    <li>Sand: 97'</li>
                                    <li>Total: 19ft</li>
                                </ul>
                            </x-card.body>
                        </x-card.card>
                    </x-flex-column>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
    </x-flex-row>
    <div class="card-group mt-3">
        <x-card.card class="mx-3">
            <x-card.header >
                <x-card.title>
                    User Friendly Controls
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">
                    <img class='card-img w-100' src="/images/controls.png" />
                    <x-card.card class="flex-shrink-1">
                        <x-card.body>Simple Levers and Hydraulic Circuits provide virtually effortless control of all functions of the mixer. </x-card.body>
                    </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
        <x-card.card class="mx-3">
            <x-card.header >
                <x-card.title>
                    Maximum Payload
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">

                    <img class='card-img' src="/images/max_payload.png" />
                    <x-card.card class="flex-shrink-1">
                        <x-card.body>Simple Levers and Hydraulic Circuits provide virtually effortless control of all functions of the mixer. </x-card.body>
                    </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>

        <x-card.card class="mx-3">
            <x-card.header >
                <x-card.title>
                    Optimized Design
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">

                    <img class='card-img' src="/images/optimized_design.png" />
                    <x-card.card class="flex-shrink-1">
                        <x-card.body>Simple Levers and Hydraulic Circuits provide virtually effortless control of all functions of the mixer. </x-card.body>
                    </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
    </div>
    <x-flex-row class="justify-content-center w-100 my-5">
        <x-card.card class="mt-5 col-5">
            <x-card.header >
                <x-card.title>
                    Your Single Source for Complete Gunite Systems
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">
                    <img class='card-img w-75 flex-shrink-1' src="/images/compressor_setup.png" />
                    <x-card.card class="flex-shrink-1">
                        <x-card.body>
                            <p>
                                <a href="{{ route('history.index') }}">We have experience in the gunite industry.</a> From contractor work to manufacturing, even going back to our franchising days. This means we know what to expect with every facet of the business and can supply new clients and seasoned veterans alike with every piece of equipment a gunite business needs, all held to the highest standards.</p> </x-card.body>
                    </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
    </x-flex-row>
</x-container-fluid>
