<x-container-fluid class="h-100 mx-0 border-0 mt-5 py-5 px-0 flex-column" bgSrc="/images/infrastruture.png">
        <x-card.card class="col-6 my-5 offset-3">
            <x-card.header class="d-flex flex-row">
                <x-card.title>
                    Our History
                </x-card.title>
                <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
            </x-card.header>
            <x-card.body class="justify-content-between p-0">
                <x-flex-row class="justify-content-between">
                    <x-flex-column class="w-50">
                        <img class='card-img m-2' src="/images/shop.png" />
                        <img class='card-img m-2' src="/images/gunite3.jpg" />
                    </x-flex-column>
                    <x-card.card class="w-50">
                        <x-card.body>
                    Brooks Strong came to Texas in 1975 and started a small concrete pouring business. By the late 70’s his experience had broadened into other businesses that included excavation and gunite.
                    The Texas concrete shortage in the late 70’s resulted in Strong acquiring his first volumetric mixers in order to maintain his own business operations. The shortcomings of the original equipment became evident and Brooks began designing and building volumetric mixers for his own use. The value of his original designs were obvious to the industry and the demand for a superior product led to the full-time manufacturing of concrete and gunite mixers. By the mid ’80s, Strong Volumetric Mixers were refined into what was recognized as the world’s best volumetric mixers. This recognition resulted from their unique user-friendly design. Simply stated, Strong Mixers have fewer moving parts, requiring less maintenance, and they do not intimidate the end user.
                    Strong’s continued commitment to product development has led to innovations in not only the volumetric mixer industry, but several other industries as well. Strong’s product lines and services include:
                    <ul>
                        <li>Continued support and service for older volumetric mixers</li>
                        <li><a href="/">Gunite Mixers</a></li>
                        <li>14 models of the <a href="https://www.superdumps.com/trailing_axles/">Strong Arm</a></li>
                        <li><a href="https://www.superdumps.com/">Super Dumps</a> (a large-payload dump truck)</li>
                        <li><a href="https://www.strongweigh.com/">StrongWeigh</a> (an on-board vehicle weight measurement scale)</li>
                        <li>An extensive fabrication and machine shop providing services to the petrochemical industry</li>
                    </ul>
                        </x-card.body>
                    </x-card.card>
                </x-flex-row>
            </x-card.body>
        </x-card.card>
</x-container-fluid>
