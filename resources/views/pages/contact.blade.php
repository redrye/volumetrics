<x-container-fluid class=" px-2 py-5" bgSrc="/images/infrastruture.png">
    <x-container class="my-4 pt-4 pb-4">
    <x-card.card class=" mb-4 mt-5">
        <x-card.header class="d-flex flex-row">
            <x-card.title>
                Contact Us
            </x-card.title>
            <x-auth-button-edit class="my-3">Submit</x-auth-button-edit>
        </x-card.header>
        <x-card.body class="card-body d-flex flex-column">
            <x-card.card class="flex-row justify-content-around align-items-center">
                <x-card.card class="text-center">
                    <h1 class="text-secondary">Address</h1><br/>
                    Volumetric Mixers by Strong, Inc.<br/>
                    13617 Ralph Culver<br/>
                    Houston,  Texas 77086
                </x-card.card>
                <x-card.card class="text-center">
                    <h1 class="text-secondary">Contact<br/>Information</h1><br/>
                    Volumetric Mixers by Strong, Inc.<br/>
                    13617 Ralph Culver<br/>
                    Houston,  Texas 77086
                </x-card.card>
            </x-card.card>
            <x-card.card class="text-center my-3">
                <h2 class="text-secondary">
                    Strong Gunite Mixers<br/>
                    The Best Mixer. The Best Service. The Best Value.
                </h2>
            </x-card.card>
            <x-card.card class="mt-2 mb-1">
                <h2 class="text-secondary">Contact Form</h2>
                <x-flex-column>
                    <x-input.text name="name" id="name_id"></x-input.text>
                    <x-flex-row>
                        <x-input.text name="email_address" id="email_address_id"></x-input.text>
                        <div class="px-1"></div>
                        <x-input.text name="phone_number" id="phone_number_id"></x-input.text>
                    </x-flex-row>
                    <x-input.text-area name="message" id="message_id"></x-input.text-area>
                        <button class="btn btn-primary offset-11" id="send">Send</button>
                </x-flex-column>
            </x-card.card>
        </x-card.body>
        </x-card.card>
    </x-container>
</x-container-fluid>
