
    <x-card.card class="h-100 position-fixed w-100">
        <div class="d-flex card-body p-0 flex-row" style=" height:80vh">
            <div class="d-flex flex-column navbar bg-transparent px-2 py-5 position-absolute" style="">
                <button id="simple_controls" class="btn btn-dark border-1 border-secondary col-12">Simple<br/>Controls</button>
                <button id="max_payload" class="btn btn-dark border-1 border-secondary col-12">Max<br/>Payload</button>
                <button id="optimized_design" class="btn btn-dark border-1 border-secondary col-12">Optimized<br/>Design</button>
                <button id="mixer_specs" class="btn btn-dark border-1 border-secondary col-12">Mixer<br/>Specs</button>
            </div>
            <div id="truck-viewer" class="w-100"></div>
            <div class="d-flex overlay text-center w-100 text-white align-items-center" style="position:absolute; height:89vh">
                <h1 class="indicator flex-fill text-dark">0%</h1>
            </div>
        </div>
    </x-card.card>
