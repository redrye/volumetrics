<x-container-fluid class="h-100 mx-0 border-0 mt-5 px-0" bgSrc="/images/infrastruture.png">
    <div class="card bg-info col-6 justify-content-end" style="margin-top: 25rem; margin-bottom: 25rem; opacity: 75%; display: none">
        <h1 class="card-title text-white offset-3 col-9 text-center" style="display: none">STRONG GUNITE SYSTEMS <br>
            <i>Leading the Industry</i></h1>
    </div>
</x-container-fluid>
<x-navbar.navbar id="navbar" class="navbar navbar-expand-lg navbar-dark bg-dark position-sticky sticky-top border-top-0" style="top: 3rem; z-index: 9999">

    <x-navbar.container>
        <x-navbar.collapse>
            <x-navbar.list class="flex-row">
                <x-navbar.nav-link href="{{ route('applications.index') }}">Applications</x-navbar.nav-link>
                <x-navbar.nav-link href="{{ route('about.index') }}">Customers</x-navbar.nav-link>
            </x-navbar.list>
        </x-navbar.collapse>
    </x-navbar.container>
</x-navbar.navbar>
<x-container-fluid class="py-5 m-0 flex-column" bgSrc="/images/mixer.png">
    <x-flex-row class="justify-content-around mb-5" style="margin-top: 10rem; margin-bottom: 5rem;">
        <x-card.info-card class="col-3" :card="$view->cards[0]" buttonhref="/history" buttontext="Our History"></x-card.info-card>
        <x-card.info-card class="col-3" :card="$view->cards[1]" buttonhref="/about/strength" buttontext="The Strong Advantage"></x-card.info-card>
        <x-card.info-card class="col-3" :card="$view->cards[2]"></x-card.info-card>
    </x-flex-row>
    <div class="card col-8 offset-2 " style="margin-top: 10rem; margin-bottom: 5rem;">
        <x-card.card :card="$view->cards[3]">
            <h1 class="card-title text-center"><i class="text-secondary">Built Strong, Built To Last</i></h1>
            <x-link-button href="/contact" class="col-4 align-self-center">Contact Us</x-link-button>
        </x-card.card>
    </div>

</x-container-fluid>
