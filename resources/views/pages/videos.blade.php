<x-container-fluid class="mt-5 pb-5 border-0 px-0 justify-content-center flex-column" bgSrc="/images/infrastruture.png">
        <x-flex-row class="justify-content-around">
            <div class="col-5 my-3">
            <div class="ratio ratio-16x9">
            <iframe  title="Smart Gunite for Guniters - V3 Software Suite - Fleet Management, Delivery Accounting, Billing" src="https://www.youtube.com/embed/rFZ52dTMsWs?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" frameborder="0"></iframe>
            </div>
            </div>
            <div class="col-5 my-3">
                <div class="ratio ratio-16x9">
            <iframe title="Swimming Pool Construction: Gunite vs Wet Shotcrete" src="https://www.youtube.com/embed/Ugq-Vmp_Ot0?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0"></iframe>
                </div>
            </div>
        </x-flex-row>
    <x-flex-row class="justify-content-around">
                <div class="col-5 my-3">
                    <div class="ratio ratio-16x9">
                <iframe title="21 to 25 yards per hour" src="https://www.youtube.com/embed/Oin3LhkdNME?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe>
                    </div>
                </div>
        <div class="col-5 my-3">
            <div class="ratio ratio-16x9">
                <iframe title="What is a Super Dump (Superdump) Dump Truck?" src="https://www.youtube.com/embed/Ijvmd11WcJc?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe>
            </div>
        </div>

            </x-flex-row>
        <x-flex-row class="justify-content-center flex-grow-1">
            <div class="col-5 my-3">
                <div class="ratio ratio-16x9">
            <iframe  title="Strong Weigh - Professional Grade Scales" src="https://www.youtube.com/embed/s877592zm6A?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe>
                </div>
            </div>
        </x-flex-row>
</x-container-fluid>

<p class="bg-dark text-white py-1 my-1">Check our YouTube page for more videos: <a href="https://www.youtube.com/channel/UCXkTEwQbBvLk0i2Ugt6VCFQ">Click Here</a>&nbsp;</p>

