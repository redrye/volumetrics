<div {{  $attributes->merge(['class' => 'card border-0'])->filter(fn($value, $key) => $key != 'card') }}>
    @isset($card)
    <meta name="card-id" content="{{$card->id}}"/>
    @if($card->title != '')
    <x-card.header>
        <x-card.title>
            {{$card->title}}
        </x-card.title>
        <x-auth-button-edit class="my-3">Edit</x-auth-button-edit>
    </x-card.header>
        @endif
    <x-card.body class="justify-content-between">
        {!! $card->contents !!}
        {{ $slot }}
    </x-card.body>
    @else
        {{ $slot }}
    @endisset
</div>
