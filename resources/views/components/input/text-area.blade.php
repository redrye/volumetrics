<x-input.group>
    <span class="input-group-text">{{ ucwords(str_replace('_', ' ', $attributes['name'])) }}</span>
    <textarea class="form-control" name="{{ $attributes['name'] }}" aria-label="{{ ucwords(str_replace('_', ' ', $attributes['name'])) }}"></textarea>
</x-input.group>
