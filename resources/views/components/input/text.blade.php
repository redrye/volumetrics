<x-input.group>
    <span class="input-group-text" id="{{ $attributes['id'] }}"><i class="fa fa-user"></i></span>
    <input type="text" class="form-control" name="{{ $attributes['name'] }}" placeholder="{{ ucwords(str_replace('_', ' ', $attributes['name'])) }}" aria-label="Name" aria-describedby="{{$attributes['id']}}">
</x-input.group>
