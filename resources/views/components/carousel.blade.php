<div id="carousel" class="carousel slide" data-bs-ride="carousel">
    <x-carousel.indicators target="carousel" num="3"></x-carousel.indicators>
    <div class="carousel-inner">
        <x-carousel.item active="true"
                         image="/images/construction.png"
                         caption-title="Intellitrucks"
                         caption-text="Truck and job management at your fingertips">
        </x-carousel.item>
        <x-carousel.item
                image="/images/construction.png"
                caption-title="Propriatary API specifically designed for load monitoring"
                caption-text="Some representative placeholder content for the second slide.">
        </x-carousel.item>
        <x-carousel.item
                image="/images/maps.png"
                caption-title="Google Maps Powered"
                caption-text="Interactive maps to view jobs in various locations.">
        </x-carousel.item>
    </div>
</div>
