<div
    class="laravel-embed__responsive-wrapper col-8"
    style="padding-bottom: {{ $aspectRatio->asPercentage() }}%"
>
    {{ $slot }}
</div>
