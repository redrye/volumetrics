import Truck from './truck';
import Mixer from './mixer';

export {
    Truck,
    Mixer
}