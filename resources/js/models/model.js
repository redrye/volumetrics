import {MTLLoader} from "three/examples/jsm/loaders/MTLLoader";
import {OBJLoader} from "three/examples/jsm/loaders/OBJLoader";

export default class Model {
    namespace = 'models';

    constructor(opts) {
        self = this;
        self.namespace = (self.namespace + '/' + self.constructor.name.toLowerCase() + 's').replaceAll('models/models', 'models');
        if (typeof opts != 'undefined') {
            Object.keys(opts).forEach(function (key) {
                self[key] = opts[key];
            })
        }
        if (self.hasOwnProperty('name')) {
            self.object = {
                url: self.namespace + '/' + self.name + '.obj',
                materials: []
            };
            self.materials = {
                url: self.namespace + '/' + self.name + '.mtl',
                get: function () {
                    new MTLLoader()
                        .load(DefaultTruck.materials.url, function (materials) {
                            Object.keys(materials.materialsInfo).forEach(function (key) {
                                if (materials.materialsInfo[key].d == 1) {
                                    materials.materialsInfo[key].d = 0;
                                }
                            })
                        });
                }
            };
        }
    }
        getMaterials() {
            return new MTLLoader()
                .load(DefaultTruck.materials.url, function (materials) {
                    Object.keys(materials.materialsInfo).forEach(function (key) {
                        if (materials.materialsInfo[key].d == 1) {
                            materials.materialsInfo[key].d = 0;
                        }
                    })
                });
        }

}

