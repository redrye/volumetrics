export default class Panel  {
    constructor( sizeX , sizeY , position ){
        var wall = new THREE.Mesh(
            new THREE.PlaneBufferGeometry( sizeX ,  sizeY ),
            new THREE.MeshStandardMaterial({
                color: 0xffffff
            })
        );
        if( position != undefined ) wall.position.copy( position );
        wall.castShadow = wall.receiveShadow = true;
        return wall;
    }
}
