import { default as MTL } from './mtl';
import { default as OBJ } from './obj';

export default {
    MTL,
    OBJ
}