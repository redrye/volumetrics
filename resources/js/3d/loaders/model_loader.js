export default class ModelLoader {
    path = 'models/'
    constructor() {

    }
    onLoad = function(object) {
            this.scene.add(object);
            this.fadeIn(object);
            return this;
    }
    onProgress = function( xhr ) {
        if ( xhr.lengthComputable ) {

            const percentComplete = xhr.loaded / xhr.total * 100;
            $('.indicator').text(Math.round( percentComplete, 2 ) + '%');
            if(percentComplete >= 99) {
                $('.indicator').parent().remove();
            }

        }
    }
    onError = function() {

    }
    fadeIn = function(object) {

    }
    fadeOut = function(object) {

    }


}