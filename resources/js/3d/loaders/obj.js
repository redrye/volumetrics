import ModelLoader from './model_loader';
export default class OBJ extends ModelLoader {
    constructor() {
        super();
        this.loader = this.loadObject()
    }
    loadObject = function() {
        var self = this;
        return new THREE.ObjectLoader(self.manager).load(self.url, self.onLoad, self.onProgress, self.onError)
    }
    loadMaterials = function() {

    }
}