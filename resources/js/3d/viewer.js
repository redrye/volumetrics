import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {RoomEnvironment} from "three/examples/jsm/environments/RoomEnvironment";
import createjs from 'createjs';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {InfoBox} from "@/3d";
import {CSS2DObject} from "three/examples/jsm/renderers/CSS2DRenderer";


let container = $('#truck-viewer');

let camera, scene, renderer, controls, raycaster, buttonPlane;

let mouseX = 0, mouseY = 0;

let windowHalfX = container.width() / 2;
let windowHalfY = container.height() / 2;

let sprites = {
    back: [],
    side: [],
    top: []
}


 function fadeIn(object) {
    object.children.forEach(function (mesh) {
        if (Array.isArray(mesh.material)) {
            mesh.material.forEach(function (child) {
                createjs.Tween.get(child)
                    .to({
                        opacity: 1,
                    }, 5000)
            })
        } else {
            createjs.Tween.get(mesh.material)
                .to({
                    opacity: 1,
                }, 5000)
        }
    })
}

 function fadeOut(materials) {
    Object.keys(materials.materialsInfo).forEach(function(key) {
        if(materials.materialsInfo[key].d == 1) {
            materials.materialsInfo[key].d = 0;
        }
    })
}

function init() {
    container = $('#truck-viewer');
    console.log(container)



    renderer = new THREE.WebGLRenderer( { antialias: true } );
    container.append( renderer.domElement );

    camera = new THREE.PerspectiveCamera( 30, container.width() / container.height(), 1, 100 );
    camera.position.z = 15;
    camera.position.y = 10;
    camera.position.x = 15;

    // scene

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xefefef );

    const pmremGenerator = new THREE.PMREMGenerator( renderer );
    scene.environment = pmremGenerator.fromScene( new RoomEnvironment() ).texture;

        const ambientLight = new THREE.AmbientLight( 0xcfcfcf, 0.8 );
       scene.add( ambientLight );


    const pointLight = new THREE.PointLight( 0xffffff, 1, 100 );
    pointLight.position.set(0, 0, 10)
    camera.add( pointLight );
    scene.add( camera );
    // manager


    const manager = new THREE.LoadingManager( function() {
        console.log('complete')
    }, function(test) {
        console.log(test);
    } );


/*
    const manager = new THREE.LoadingManager( loadModel );

    manager.onProgress = function ( item, loaded, total ) {

        console.log( item, loaded, total );

    };*/

    // model

    function onProgress( xhr ) {

        if ( xhr.lengthComputable ) {

            const percentComplete = xhr.loaded / xhr.total * 100;
            $('.indicator').text(Math.round( percentComplete, 2 ) + '%');
            if(percentComplete >= 99) {
                $('.indicator').parent().remove();
            }

        }

    }

    function onError() {}

/*    // Lines
    const points = [];
    points.push( new THREE.Vector3( - 10, 0, 0 ) );
    points.push( new THREE.Vector3( 0, 10, 0 ) );
    points.push( new THREE.Vector3( 10, 0, 0 ) );

    const material = new THREE.LineBasicMaterial( { color: 0x0000ff } );

    const geometry = new THREE.BufferGeometry().setFromPoints( points );


    const line = new THREE.Line(geometry, material)

    scene.add(line)*/


    // Anotation


    // Interface
    var mixer = {};
    new MTLLoader()
        .load( 'models/Kenworth/Mixer.mtl', function(materials) {
            fadeOut(materials)
            new OBJLoader( manager )
                .setMaterials(materials)
                .load( 'models/Kenworth/Mixer.obj', function ( object ) {
                    mixer = object
                    scene.add(mixer);
                });
        });


    var MTL = new MTLLoader()
    MTL.load( 'models/Kenworth/Truck.mtl', function(materials) {
            fadeOut(materials)
            return new OBJLoader(manager)
                .setMaterials(materials)
                .load('models/Kenworth/Truck.obj', function (truck) {
                    scene.add(truck);
                    fadeIn(mixer);
                   fadeIn(truck);
                    }, onProgress, onError);
                });
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( container.width(), container.height() );
    controls = new OrbitControls(camera, renderer.domElement)

    $('#simple_controls').on('click', function(e) {
        Object.keys(sprites).forEach(function(key) {
            for(var i = 0; i < sprites[key].length; i++) {
                scene.remove(sprites[key][i]);
            }
        })

        sprites.side[0] = new InfoBox( "Simple Levers and Hydraulic Circuits\nprovide virtually effortless control of\nall functions of the mixer.", {
            fontsize: 10
        });
        camera.add(sprites.side[0])
        sprites.side[0].position.set(-0.5 ,0, -4);
        createjs.Tween.get(camera.position)
            .to({
                    x: 1,
                    y: 3,
                    z: -6
            }, 500)
            .call(function(element) {
                createjs.Tween.get(sprites.side[0].material)
                        .to({
                            opacity: 1,
                            visible: true
                        }, 1000)
                });
            });

        // camera.position.set(15, 4, 0)
    $('#max_payload').on('click', function() {
        Object.keys(sprites).forEach(function (key) {
            for (var i = 0; i < sprites[key].length; i++) {
                scene.remove(sprites[key][i]);
            }
        })

//        camera.position.set(15, 4, 0)
        sprites.back[0] = new InfoBox("Strong Gunite Mixers are the only gunite\nmachines designed to accept a Strong Arm\ntrailing axle. This option allows heavier\npayloads while conforming to state and federal\nweight restrictions. Our familiarity with each\nstate’s regulations can ensure your business\ngets the maximum efficiency out of each and\nevery truckload of gunite.");
        sprites.back[0].position.set(1, 2, -3);
        scene.add(sprites.back[0])
         createjs.Tween.get(camera.position)
            .to({
                x: 0,
                y: 3,
                z: -6
            }, 500)
             .call(function() {
                 createjs.Tween.get(sprites.back[0].material)
                     .to({
                         opacity: 1,
                         visible: true
                     }, 1000)
             });


    })

    $('#optimized_design').on('click', function() {
        Object.keys(sprites).forEach(function (key) {
            for (var i = 0; i < sprites[key].length; i++) {
                scene.remove(sprites[key][i]);
            }
        })

//        camera.position.set(15, 4, 0)
        sprites.back[0] = new InfoBox("Finite Element Analysis software enabled the\nengineering of the lightest, but strongest, mixer\nbody on the market. Decades of working closely\nwith our clients honed and refined this design\ninto the mixer body that has dominated the\ngunite industry.");
        sprites.back[0].position.set(-1, 3, -6);
        scene.add(sprites.back[0])
        createjs.Tween.get(camera.position)
            .to({
                x: -2,
                y: 4,
                z: -8
            }, 500)
            .call(function() {
                createjs.Tween.get(sprites.back[0].material)
                    .to({
                        opacity: 1,
                        visible: true
                    }, 1000)
            });


    })

    $('#optimized_design').on('click', function() {
        Object.keys(sprites).forEach(function (key) {
            for (var i = 0; i < sprites[key].length; i++) {
                scene.remove(sprites[key][i]);
            }
        })

//        camera.position.set(15, 4, 0)
        sprites.back[0] = new InfoBox("Finite Element Analysis software enabled the\nengineering of the lightest, but strongest, mixer\nbody on the market. Decades of working closely\nwith our clients honed and refined this design\ninto the mixer body that has dominated the\ngunite industry.");
        sprites.back[0].position.set(-1, 3, -6);
        scene.add(sprites.back[0])
        createjs.Tween.get(camera.position)
            .to({
                x: -2,
                y: 4,
                z: -8
            }, 500)
            .call(function() {
                createjs.Tween.get(sprites.back[0].material)
                    .to({
                        opacity: 1,
                        visible: true
                    }, 1000)
            });


    })



    //

    /*
        window.addEventListener( 'resize', onWindowResize );
    */

}


function onWindowResize() {

    camera.aspect = container.width() / container.height();
    camera.updateProjectionMatrix();

    renderer.setSize( container.width(), container.height() );

}

function onDocumentMouseMove( event ) {

    mouseX = ( event.clientX - windowHalfX ) / 2;
    mouseY = ( event.clientY - windowHalfY ) / 2;

}


//

function animate() {
    requestAnimationFrame( animate );
 //   controls.update()
    render();
}

function render() {
    camera.lookAt( scene.position );
//    controls.update();
    renderer.render( scene, camera );
}

export default {
    init,
    animate
}
