
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {DRACOLoader} from "three/examples/jsm/loaders/DRACOLoader";
import {RoomEnvironment} from "three/examples/jsm/environments/RoomEnvironment";

let container = $('#truck-viewer');

let camera, scene, renderer, controls, grid;

let mouseX = 0, mouseY = 0;

let windowHalfX = container.width() / 2;
let windowHalfY = container.height() / 2;

let object;



function init() {
    container = $('#truck-builder');
    console.log(container)

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    container.append( renderer.domElement );

    camera = new THREE.PerspectiveCamera( 20, container.width() / container.height(), 1, 100 );
    camera.position.z = 15;
    camera.position.y = 10;
    camera.position.x = 15;

    // scene

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xeeeeee );

    const pmremGenerator = new THREE.PMREMGenerator( renderer );
    scene.environment = pmremGenerator.fromScene( new RoomEnvironment() ).texture;

    grid = new THREE.GridHelper( 1000, 400, 0x000000, 0x000000 );
    grid.material.opacity = 0.1;
    grid.material.depthWrite = false;
    grid.material.transparent = true;
    scene.add( grid );
/*
    const ambientLight = new THREE.AmbientLight( 0xcccccc, 1 );
    scene.add( ambientLight );
*/


    const pointLight = new THREE.HemisphereLight( 0xffffff, 0x222222, 0.999 );
    camera.add( pointLight );
    scene.add( camera );

    // manager

    function loadModel() {

        object.traverse( function ( child ) {

          //  if ( child.isMesh ) child.material.map = texture;

        } );

        object.position.y = - 0;
        scene.add( object );

    }

    const manager = new THREE.LoadingManager( loadModel );

    manager.onProgress = function ( item, loaded, total ) {

        console.log( item, loaded, total );

    };

    // model

    function onProgress( xhr ) {

        if ( xhr.lengthComputable ) {

            const percentComplete = xhr.loaded / xhr.total * 100;
            console.log( 'model ' + Math.round( percentComplete, 2 ) + '% downloaded' );

        }

    }

    function onError() {}


   new MTLLoader()
        .load( 'models/Kenworth/Kenworth.mtl', function(materials) {
        materials.preload();
        new OBJLoader( manager )
            .setMaterials(materials)
            .load( 'models/Kenworth/Kenworth.obj', function ( obj ) {
                object = obj;
                scene.add(object)
            }, onProgress, onError );
    });


    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( container.width() * 2 , container.height() * 2 );
    controls = new OrbitControls(camera, renderer.domElement)

/*
    document.addEventListener( 'mousemove', onDocumentMouseMove );
*/

    //

/*
    window.addEventListener( 'resize', onWindowResize );
*/

}

function onWindowResize() {

    camera.aspect = container.width() / container.height();
    camera.updateProjectionMatrix();

    renderer.setSize( container.width(), container.height() );

}

function onDocumentMouseMove( event ) {

    mouseX = ( event.clientX - windowHalfX ) / 2;
    mouseY = ( event.clientY - windowHalfY ) / 2;

}

//

function animate() {
    requestAnimationFrame( animate );
    render();
}

function render() {
    camera.lookAt( scene.position );
    controls.update();
    renderer.render( scene, camera );
}

export default {
    init,
    animate
}
