export default class InfoBox {
    fontface = "Verdana";
    fontsize = 16;
    border_size = 4;
    border_color = {r: 0, g: 0, b: 0, a: 1.0};
    background_color = {r: 0, g: 0, b: 0, a: 1.0};

    constructor(msg, opts) {
        return this.makeTextSprite(msg, opts)
    }
        makeTextSprite(message, parameters) {
            if (parameters === undefined) parameters = {};

            var fontface = parameters.hasOwnProperty("fontface") ?
                parameters["fontface"] : "Verdana";

            var fontsize = parameters.hasOwnProperty("fontsize") ?
                parameters["fontsize"] : 10;

            var borderThickness = parameters.hasOwnProperty("borderThickness") ?
                parameters["borderThickness"] : 4;

            var borderColor = parameters.hasOwnProperty("borderColor") ?
                parameters["borderColor"] : {r: 0, g: 0, b: 0, a: 1.0};

            var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
                parameters["backgroundColor"] : {r: 0, g: 0, b: 0, a: 1.0};

            // var spriteAlignment = THREE.SpriteAlignment.topLeft;

            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            context.font = fontsize + "px " + fontface;

            // get size data (height depends only on font size)
            var lines = message.split('\n');


            var metrics = context.measureText(lines.reduce(function (a, b) {
                    return a.length > b.length ? a : b;
                }
            ));
            var textWidth = metrics.width;

            // background color
            context.fillStyle = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
                + backgroundColor.b + "," + backgroundColor.a + ")";
            // border color
            context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
                + borderColor.b + "," + borderColor.a + ")";


            context.lineWidth = fontsize * 2;
            this.roundRect(context, borderThickness, borderThickness, textWidth + (borderThickness * 2), (fontsize * lines.length), 0);

            // text color
            context.fillStyle = "rgba(255, 255, 255, 1.0)";


            for (var i = 0; i < lines.length; i++) {
                context.fillText(lines[i], borderThickness, ((i + 1) * fontsize) + borderThickness);
            }
            // canvas contents will be used for a texture
            var texture = new THREE.Texture(canvas)
            texture.needsUpdate = true;

            var spriteMaterial = new THREE.SpriteMaterial(
                {map: texture, opacity: 0});
            var sprite = new THREE.Sprite(spriteMaterial);
            sprite.scale.set(1, 1, 1);
            return sprite;
        }


     roundRect(ctx, x, y, w, h, r) {
        ctx.beginPath();
        ctx.moveTo(x + r, y);
        ctx.lineTo(x + w - r, y);
        ctx.quadraticCurveTo(x + w, y, x + w, y + r);
        ctx.lineTo(x + w, y + h - r);
        ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
        ctx.lineTo(x + r, y + h);
        ctx.quadraticCurveTo(x, y + h, x, y + h - r);
        ctx.lineTo(x, y + r);
        ctx.quadraticCurveTo(x, y, x + r, y);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
}
