import {View} from "View";
export default {
    init: function() {
        $('.navbar').find('a:not(".dropdown-toggle"):not(".link")').on('click', function(e) {
            e.preventDefault();
            var url = e.currentTarget.href;
            var scriptPath = (new URL(url)).pathname.substr(1).split('/');
            for(var i = 0; i < scriptPath.length; i++) {
                scriptPath[i] = scriptPath[i][0].toUpperCase() + scriptPath[i].substr(1)
            }
            View.load(url, function() {
                window.scrollTo(0, 0)
                   View.loadScript(scriptPath)
            })
        })
    }
}
