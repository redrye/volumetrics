import {Truck, View} from "View";
import  * as Script from 'View';
export default {
    init: function() {
        $('.card-body, p').find('a').on('click', function(e) {
            e.preventDefault();
            window.scrollTo(0, 0)
            var url = e.currentTarget.href;
            var scriptPath = (new URL(url)).pathname.substr(1).split('/');
            for(var i = 0; i < scriptPath.length; i++) {
                scriptPath[i] = scriptPath[i][0].toUpperCase() + scriptPath[i].substr(1)
            }
            View.load(url, function() {
                View.loadScript(scriptPath)
            })
        })
    }
}
