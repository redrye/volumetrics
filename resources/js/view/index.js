import Home from 'View/home';
import Truck from 'View/truck';
import About from 'View/about';
import Contact from 'View/contact'
import Applications from 'View/applications';
import Mixers from 'View/mixers';
import Gunite from '../gunite'
import * as Script from 'View';

var View = $('main')
View.loadScript = function(scriptPath) {
    if (scriptPath.length == 1) {
        if (typeof Script[scriptPath[0]] != 'undefined') {
            Script[scriptPath[0]].init()
        }
    } else {
        console.log(Script[scriptPath[0]])
        if (typeof Script[scriptPath[0]][scriptPath[1]] != 'undefined') {
            console.log(Script[scriptPath[0]])
            Script[scriptPath[0]][scriptPath[1]].init()
        }
    }
}
export {
    Home,
    View,
    Truck,
    About,
    Contact,
    Applications,
    Mixers,
    Gunite
}
