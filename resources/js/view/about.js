import {Gunite, View} from "View";
import * as Script from "View";
import Truck from "View/truck";

export default {
    init: function() {

    },
    Strengths: {
        init: function() {
            Truck.Viewer.init()
            window.scrollTo(0,0)
            $('div.card').find('a').off().on('click', function(e) {
                e.preventDefault();
                window.scrollTo(0, 0)
                var url = e.currentTarget.href;
                var scriptPath = (new URL(url)).pathname.substr(1).split('/');
                View.load(url, function() {
                    View.loadScript(scriptPath)
                })
            })
        }
    },
    Gunite:  {
        init: function() {
            window.scrollTo(0,0)
            Gunite.init()
        }
    }
}
