import {Home} from "../view";
import {Navbar} from "../navigation";

export default {
    init: function() {
       Home.init();
        $('.navbar').find('a:not(".dropdown-toggle"):not(".link")').off();
        Navbar.init()
    }
}
