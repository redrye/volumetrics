import {Viewer, Builder} from '../3d'
export default {
    Builder: {
        init: function() {
        //    console.log('inside builder function')
            Builder.init();
            Builder.animate();
        }
    },
    Viewer: {
        init: function() {
            //    console.log('inside builder function')
            Viewer.init();
            Viewer.animate();

        }
    }
}
