export default {
    init: function () {
        window.scrollTo(0,0)
        $('.btn-primary').on('click', function (e) {
            e.preventDefault();
            self = $(this);
            $.ajax({
                url: '/contact',
                data: self.parent().parent().find('.form-control').serialize(),
                method: 'POST',
                success: function(data) {
                    console.log(data);
                },
                error: function(data) {
                    console.log(data);
                }
            })
        })
    }
}
