<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MixerController;
use App\Http\Controllers\VideoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::resources([
//    '/' => HomeController::class,
 //   '/home' => HomeController::class,
//    '/about' => AboutController::class,
    '/history' => HistoryController::class,
//    '/videos' => VideoController::class,
    '/cards' => \App\Http\Controllers\CardController::class
 ]);
Route::get('/mixers', MixerController::class)->name('mixers.index');
Route::get('/', HomeController::class);
Route::post('/', HomeController::class);
Route::get('/contact',ContactController::class)->name('contact.index');
Route::post('/contact',EmailController::class);

Route::get('/home', HomeController::class)->name('home.index');
Route::get('/about/index', [AboutController::class, 'index'])->name('about.index');
Route::get('/about/gunite', [AboutController::class, 'gunite'])->name('about.gunite');
Route::get('/about/strengths', [AboutController::class, 'strengths'])->name('about.strengths');
Route::get('/videos', VideoController::class)->name('videos.index');
Route::get('/applications', function() {
   return view('pages.applications');
})->name('applications.index');
