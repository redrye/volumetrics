const path = require('path');
module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
            'Navigation': path.resolve('resources/js/navigation'),
            'View': path.resolve('resources/js/view'),
            createjs: 'createjs/builds/createjs-2015.11.26.combined'
        },
    },
    module: {
        rules: [
            {
                test: /node_modules[/\\]createjs/,
                use: [
                    {
                        loader: 'exports-loader',
                        options: {
                            type: 'commonjs',
                            exports: 'single window.createjs'
                        },
                    }]
            },
            {
                test: /node_modules[/\\]createjs/,
            use: [{
                        loader: 'imports-loader',
                        options: {
                            wrapper: 'window'
                        }
                    },
                ]
            }
        ]
    },
};
